/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100503
 Source Host           : localhost:3306
 Source Schema         : ruby-ss

 Target Server Type    : MySQL
 Target Server Version : 100503
 File Encoding         : 65001

 Date: 23/02/2021 16:20:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_appt
-- ----------------------------
DROP TABLE IF EXISTS `tb_appt`;
CREATE TABLE `tb_appt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pend_appt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `appt_rmdr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expire_ts` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `update_ts` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `create_ts` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `cust_rid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_rid`(`cust_rid`) USING BTREE,
  CONSTRAINT `cust_rid` FOREIGN KEY (`cust_rid`) REFERENCES `tb_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_appt
-- ----------------------------
INSERT INTO `tb_appt` VALUES (16, '密码过期了', '哈哈', '2021-02-26 15:01:32', '2021-02-23 15:01:38', '2021-02-23 15:01:41', 1);
INSERT INTO `tb_appt` VALUES (17, '困了', '呵呵', '2021-02-27 15:02:22', '2021-02-23 15:02:25', '2021-02-23 15:02:28', 1);

SET FOREIGN_KEY_CHECKS = 1;
