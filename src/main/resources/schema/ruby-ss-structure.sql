CREATE DATABASE  IF NOT EXISTS `ruby-ss` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `ruby-ss`;
-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ruby-ss
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_bill`
--

DROP TABLE IF EXISTS `tb_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `total` int(255) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `tb_customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_bill`
--

LOCK TABLES `tb_bill` WRITE;
/*!40000 ALTER TABLE `tb_bill` DISABLE KEYS */;
INSERT INTO `tb_bill` VALUES (1,'a','2021-02-18 00:00:00',100,1),(2,'aa','2021-02-18 00:00:00',200,1),(3,'aaaa','2021-02-18 00:00:00',300,1),(4,'aaaaa','2021-02-18 00:00:00',400,1),(5,'aaaaaa','2021-02-18 00:00:00',500,1),(6,'aaaaaaa','2021-02-18 00:00:00',600,1),(7,'b','2021-02-20 00:00:00',1,2),(8,'bb','2021-02-20 00:00:00',2,2),(9,'bbb','2021-02-20 00:00:00',3,2),(10,'bbbb','2021-02-20 00:00:00',4,2),(11,'bbbbb','2021-02-20 00:00:00',5,2),(12,'c','2021-02-21 00:00:00',10,3),(13,'cc','2021-02-21 00:00:00',20,3),(14,'ccc','2021-02-21 00:00:00',30,3),(15,'cccc','2021-02-21 00:00:00',40,3);
/*!40000 ALTER TABLE `tb_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_customer`
--

DROP TABLE IF EXISTS `tb_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `age` int(11) NOT NULL,
  `vip` smallint(1) NOT NULL DEFAULT '0',
  `login_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `login_password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_customer`
--

LOCK TABLES `tb_customer` WRITE;
/*!40000 ALTER TABLE `tb_customer` DISABLE KEYS */;
INSERT INTO `tb_customer` VALUES (1,'johnno',20,1,'test','test','2021-02-19 00:00:00','13800000000','13800000000@qq.com'),(2,'johnno2',21,2,'test2','test2','2021-02-19 00:00:00','13800000002','13800000002@qq.com'),(3,'johnno3',22,2,'test3','test3','2021-02-19 00:00:00','13800000003','13800000003@qq.com');
/*!40000 ALTER TABLE `tb_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_demo`
--

DROP TABLE IF EXISTS `tb_demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='demo table for test';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_demo`
--

LOCK TABLES `tb_demo` WRITE;
/*!40000 ALTER TABLE `tb_demo` DISABLE KEYS */;
INSERT INTO `tb_demo` VALUES (1,'build at 20210222','2021-02-22 15:21:23',1),(2,'buid data with mysql workbench ','2021-02-22 15:21:55',2),(3,'use for ruby SS','2021-02-22 15:22:13',3);
/*!40000 ALTER TABLE `tb_demo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-22 15:35:37
