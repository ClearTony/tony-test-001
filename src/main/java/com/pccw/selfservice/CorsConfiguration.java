package com.pccw.selfservice;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CorsConfiguration implements WebMvcConfigurer {
	
	public void addCorsMappings(CorsRegistry registry) {
		//Todo: define specific addresses
		registry.addMapping("/**")
		.allowedOrigins("*")
		.allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS");
	}
}
