package com.pccw.selfservice.bean.dto;

import java.util.Date;

import lombok.Data;
@Data
public class DemoDTO {
    private int id;
    private String msg;
    private Date date;
    private int number;

}
