package com.pccw.selfservice.bean.dto;

import java.util.Date;

import com.pccw.selfservice.entity.DBCustomer;

import lombok.Data;

@Data
public class BillDTO {
	private int id;
	private String message;
	private Date createDate;
	private int total;
	private String customerId;
	private DBCustomer customer;

}
