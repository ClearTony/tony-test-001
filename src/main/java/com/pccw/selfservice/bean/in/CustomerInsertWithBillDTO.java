package com.pccw.selfservice.bean.in;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pccw.selfservice.entity.DBBill;

import lombok.Data;

@Data
public class CustomerInsertWithBillDTO implements Serializable {
	private int id;
	private String name;
	private int age;
	private int vip;
	private String loginName;
	private String loginPassword;
	private Date createDate;
	private String phone;
	private String email;
	private List<DBBill> billList;
}
