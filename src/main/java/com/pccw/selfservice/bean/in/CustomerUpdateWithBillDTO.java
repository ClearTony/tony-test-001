package com.pccw.selfservice.bean.in;

import java.util.List;

import com.pccw.selfservice.entity.DBBill;

import lombok.Data;

@Data
public class CustomerUpdateWithBillDTO {
	private int id;
	private List<DBBill> billList;
}
