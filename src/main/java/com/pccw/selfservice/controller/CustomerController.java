package com.pccw.selfservice.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pccw.selfservice.bean.in.CustomerInsertWithBillDTO;
import com.pccw.selfservice.bean.in.CustomerUpdateWithBillDTO;
import com.pccw.selfservice.entity.DBCustomer;
import com.pccw.selfservice.service.CustomerService;

import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Api(value = "Customer module",tags = {"Customer module"})
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/customer", produces = "application/json")
@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("/getAllCustomer")
	public ArrayList<DBCustomer> getAllCustomer() {
		ArrayList<DBCustomer> customerList = (ArrayList<DBCustomer>) customerService.getAllCustomer();
		return customerList;
	}
	
	@PostMapping("/insertCustomer")
	public DBCustomer insertCustomer(@RequestBody CustomerInsertWithBillDTO data) {
		DBCustomer customer = customerService.insertCustomerWithBillList(data);
		return customer;
	}
	
	@PostMapping("/updateCustomer")
	public DBCustomer insertCustomer(@RequestBody CustomerUpdateWithBillDTO data) {
		DBCustomer customer = customerService.updateCustomerWithBillList(data);
		return customer;
	}

}
