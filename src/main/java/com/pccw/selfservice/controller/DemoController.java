package com.pccw.selfservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pccw.selfservice.bean.dto.DemoDTO;
import com.pccw.selfservice.service.DemoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Api(value = "Demo module",tags = {"Demo module"})
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/demo", produces = "application/json")
@RestController
public class DemoController {
	
	@Autowired
	private DemoService demoService;
	
	@ApiOperation(value = "check if Demo module works", notes = "")
	@GetMapping("/check")
	public String print() {
		return "hello Self-Service demo!";
	}
	@GetMapping("/findAll")
	public List<DemoDTO> getAll(){
		return demoService.showAllDemo();
	}

}
