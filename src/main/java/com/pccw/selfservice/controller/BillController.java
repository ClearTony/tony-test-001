package com.pccw.selfservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pccw.selfservice.entity.DBBill;
import com.pccw.selfservice.service.BillService;

import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Api(value = "Bill module",tags = {"Bill module"})
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/bill", produces = "application/json")
@RestController
public class BillController {

	@Autowired
	private BillService billService;

	@GetMapping("/check")
	public String print() {
		return "bill module work!";
	}

	@GetMapping("/getAllBill")
	public List<DBBill> getBillList() {
		return billService.getAllBill();
	}

}
