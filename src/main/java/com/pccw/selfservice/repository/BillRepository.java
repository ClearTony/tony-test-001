package com.pccw.selfservice.repository;

import java.util.List;

import com.pccw.selfservice.entity.DBBill;

public interface BillRepository extends BaseRepository<DBBill, Integer> {
	
	List<DBBill> findByCustomerId(int CustomerId);

}
