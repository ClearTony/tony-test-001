package com.pccw.selfservice.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author johnno
 *
 * @param <T>
 * @param <ID>
 * 
 *             to use repository with <b>Paging function</b> and <b>Common
 *             function</b></br>
 *             please create repository extends this interface <br>
 *             <br>
 * 
 *             Example:<br>
 *             <code>public interface CustomerRepository extends BaseRepository<DBCustomer, Integer> {}</code>
 * 
 * 
 */
@NoRepositoryBean
public interface BaseRepository<T, ID> extends PagingAndSortingRepository<T, ID> {

}
