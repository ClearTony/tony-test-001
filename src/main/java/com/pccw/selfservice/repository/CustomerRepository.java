package com.pccw.selfservice.repository;

import java.util.Optional;

import com.pccw.selfservice.entity.DBCustomer;

public interface CustomerRepository extends BaseRepository<DBCustomer, Integer> {
	
	Optional<DBCustomer> findByName(String name);

}
