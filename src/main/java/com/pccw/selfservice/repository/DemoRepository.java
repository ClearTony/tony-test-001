package com.pccw.selfservice.repository;

import java.util.List;

import com.pccw.selfservice.entity.DBDemo;

public interface DemoRepository extends BaseRepository<DBDemo, Integer> {
	
	List<DBDemo> findByMsg(Integer msg);

}
