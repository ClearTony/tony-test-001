package com.pccw.selfservice.service;

import java.util.List;

import com.pccw.selfservice.bean.in.CustomerInsertWithBillDTO;
import com.pccw.selfservice.bean.in.CustomerUpdateWithBillDTO;
import com.pccw.selfservice.entity.DBCustomer;

public interface CustomerService extends BaseSevice {
	List<DBCustomer> getAllCustomer();
	DBCustomer insertCustomerWithBillList(CustomerInsertWithBillDTO data );
	DBCustomer updateCustomerWithBillList(CustomerUpdateWithBillDTO data);
}
