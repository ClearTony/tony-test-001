package com.pccw.selfservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pccw.selfservice.entity.DBBill;
import com.pccw.selfservice.repository.BillRepository;
import com.pccw.selfservice.service.BillService;

import lombok.extern.log4j.Log4j2;
@Log4j2
@Service
public class BillServiceimpl extends BaseServiceImpl implements BillService {
	
	@Autowired
	private BillRepository billRepository;
	

	public List<DBBill> getAllBill() {
		ArrayList<DBBill> billList = (ArrayList<DBBill>) billRepository.findAll();
		log.info(billList.toString());
		return billList;
	}

}
