package com.pccw.selfservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pccw.selfservice.bean.in.CustomerInsertWithBillDTO;
import com.pccw.selfservice.bean.in.CustomerUpdateWithBillDTO;
import com.pccw.selfservice.entity.DBBill;
import com.pccw.selfservice.entity.DBCustomer;
import com.pccw.selfservice.repository.BillRepository;
import com.pccw.selfservice.repository.CustomerRepository;
import com.pccw.selfservice.service.CustomerService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Transactional
public class CustomerServiceImpl extends BaseServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private BillRepository billRepository;

	public List<DBCustomer> getAllCustomer() {
		ArrayList<DBCustomer> customerList=(ArrayList<DBCustomer>) customerRepository.findAll();
		return customerList;
	}

	public DBCustomer insertCustomerWithBillList(CustomerInsertWithBillDTO data ) {
		DBCustomer customer = new DBCustomer();
		BeanUtils.copyProperties(data, customer);
		customer.setCreateDate(new java.util.Date());
		log.info("customer"+customer.toString());
		customerRepository.save(customer);
		billRepository.saveAll(customer.getBillList());
		log.info("customer"+customer.toString());
		return customer;
	}

	
	public DBCustomer updateCustomerWithBillList(CustomerUpdateWithBillDTO data) {
		DBCustomer customer = customerRepository.findById(data.getId()).get();
		log.info("customer"+customer.toString());
		List<DBBill> billList =customer.getBillList();
		billList.addAll(data.getBillList());
		customerRepository.save(customer);
		//billRepository.saveAll(customer.getBillList());
		log.info("customer"+customer.toString());
		return customer;
	}

}
