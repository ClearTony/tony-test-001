package com.pccw.selfservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pccw.selfservice.bean.dto.DemoDTO;
import com.pccw.selfservice.entity.DBDemo;
import com.pccw.selfservice.repository.DemoRepository;
import com.pccw.selfservice.service.DemoService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class DemoServiceImpl extends BaseServiceImpl implements DemoService {
	
	@Autowired
	private DemoRepository demoRepository;

	public List<DemoDTO> showAllDemo() {
		log.info("show all demo with list");
		ArrayList<DBDemo> demoList = (ArrayList<DBDemo>) demoRepository.findAll();
		ArrayList<DemoDTO> returnList =new ArrayList<DemoDTO>();
		for (DBDemo dBDemo : demoList) {
			DemoDTO demoDTO =new DemoDTO();
			BeanUtils.copyProperties(dBDemo, demoDTO);
			returnList.add(demoDTO);
		}
		return returnList;
	}

}
