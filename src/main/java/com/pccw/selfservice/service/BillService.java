package com.pccw.selfservice.service;

import java.util.List;

import com.pccw.selfservice.entity.DBBill;

public interface BillService extends BaseSevice {
	
	List<DBBill> getAllBill();

}
