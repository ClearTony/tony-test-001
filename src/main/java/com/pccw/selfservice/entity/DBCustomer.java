package com.pccw.selfservice.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@Entity
@Table(name = "tb_customer")
@ToString(exclude = {"billList"})
@EqualsAndHashCode(exclude = {"billList"})
public class DBCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "age")
	private int age;
	@Column(name = "vip")
	private int vip;
	@Column(name = "login_name")
	private String loginName;
	@Column(name = "login_password")
	private String loginPassword;
	@Column(name = "create_date")
	private Date createDate;
	@Column(name = "phone")
	private String phone;
	@Column(name = "email")
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private List<DBBill> billList ;
	
	
}
