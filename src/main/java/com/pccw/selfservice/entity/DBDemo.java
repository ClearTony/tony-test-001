package com.pccw.selfservice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="tb_demo")
public class DBDemo {
	@Id
    @GeneratedValue
    private int id;
	@Column(name = "msg")
    private String msg;
	@Column(name = "date")
    private Date date;
	@Column(name = "number")
    private int number;

}
